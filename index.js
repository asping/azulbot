// Library to provide natual language processing and
// basic machine learning cabailities
const NLP = require('natural');

// Library to enrich the training data from the conversation
var NLPLearn = require('compromise');

// Provides a wrapper around the Slack API to easily
// interact with the chat service
const BotKit = require('botkit');

// Built-in Node library for loading files operations
const fs = require('fs');

// Load our environment variables from the .env file
require('dotenv').config();

// Create a new classifier to train
const classifier = new NLP.LogisticRegressionClassifier();

// What are the types of chats we want to consider
// In this case, we only care about chats that come directly to the bot
const scopes = [
    'direct_mention',
    'direct_message',
    'mention'
];

// Get our Slack API token from the environment
const token = process.env.SLACK_API_TOKEN;

// Create a chatbot template that can be instantiated using Botkit
const Bot = BotKit.slackbot({
    debug: false,
    storage: undefined
});

/**
 * NEW: Function to enrich the training data so that we can learn from our conversations
 * 
 * @param {String} filePath 
 */
function learn(filePath) {
    let rawdata = fs.readFileSync('./trainingData.json');  
    let trainingData = JSON.parse(rawdata);  
    console.log(student); 
}

/**
 * Function to easily parse a given json file to a JavaScript Object
 * 
 * @param {String} filePath 
 * @returns {Object} Object parsed from json file provided
 */
function parseTrainingData(filePath) {
    const trainingFile = fs.readFileSync(filePath);
    return JSON.parse(trainingFile);
}

/**
 * Will add the phrases to the provided classifier under the given label.
 * 
 * @param {Classifier} classifier
 * @param {String} label
 * @param {Array.String} phrases
 */
function trainClassifier(classifier, label, phrases) {
    //console.log('Teaching set', label, phrases);
    phrases.forEach((phrase) => {
        //console.log(`Teaching single ${label}: ${phrase}`);
        classifier.addDocument(phrase.toLowerCase(), label);
    });
}

/**
 * Uses the trained classifier to give a prediction of what
 * labels the provided pharse belongs to with a confidence
 * value associated with each and a a guess of what the actual
 * label should be based on the minConfidence threshold.
 * 
 * @param {String} phrase 
 * 
 * @returns {Object}
 */
function interpret(phrase) {
    console.log('interpret', phrase);
    const guesses = classifier.getClassifications(phrase.toLowerCase());
    console.log('guesses', guesses);
    const guess = guesses.reduce((x, y) => x && x.value > y.value ? x : y);
    
    //checking which nouns and verbs are being used in the sentence
    //let doco = NLPLearn(phrase);
    //console.log('This is the test:', doco.sentences().verbs().out('array'));
    //console.log('This is the test:', doco.sentences().nouns().out('array'));
    return {
        probabilities: guesses,
        guess: guess.value > (0.7) ? guess.label : null
    };
}

/**
 *  Adds a new concept in the training data
 * @param {String} concept 
 * @param {String} question 
 * @param {String} answer 
 */
function addTrainingData(concept, question, answer){
    //trainingData={...trainingData, concept : {"questions": [question],"answer": answer}};
    trainingData[concept] = {"questions": [question],"answer": answer};
    //write the training data into the file
    var writeJson = JSON.stringify(trainingData);
    fs.writeFileSync('./trainingData.json',writeJson);
}

/**
 * Callback function for BotKit to call. Provided are the speech
 * object to reply and the message that was provided as input.
 * Function will take the input message, attempt to label it 
 * using the trained classifier, and return the corresponding
 * answer from the training data set. If no label can be matched
 * with the set confidence interval, it will respond back saying
 * the message was not able to be understood.
 * 
 * @param {Object} speech 
 * @param {Object} message 
 */
function handleMessage(speech, message) {
    const interpretation = interpret(message.text);
    console.log('InternChatBot heard: ', message.text);
    console.log('InternChatBot interpretation: ', interpretation);

    if (!isLearning) {
        if (interpretation.guess && trainingData[interpretation.guess]) {
            console.log('Found response');
            let doco = NLPLearn(message.text);
            //summary(message.text);
            speech.reply(message, trainingData[interpretation.guess].answer);
        } else {
            console.log('Couldn\'t match phrase')
            //let trainingData = parseTrainingData("./trainingData.json")
            //console.log('This is the current training data:', trainingData);
            speech.reply(message, 'Sorry, I\'m not sure what you mean. You can help me by telling me what a good answer \
            to that question is');
            isLearning = true;
            let doco = NLPLearn(message.text);
            concept = extractConcept(doco);
            //summary(message.text);
            question = message.text;
            console.log('==>THIS IS THE VALUE OF THE CONCEPT: ',concept,' AND THE QUESTION: ',question);
        }
    } else {
        try{
            addTrainingData(concept, question, message.text);
            isLearning = false;
            speech.reply(message,'Great, thanks. I have added it to my knowledge');
            train();
        }
        catch(err){
            speech.reply(message, 'Woah! Looks like I had an internal error... Let\'s keep trying');
            isLearning = false;
        }
    }
}

/**
 * This function extracts the concept we want to use to gather the knowledge.
 * @param {String} message
 */
function extractConcept (doco){
    let concepts_nouns = doco.sentences().nouns().out('array');
    concept = "";

    //add the nouns to create the concept
    console.log("Length of the array", concepts_nouns.length);
    for(var i=0; i<concepts_nouns.length; i++) {
        let concept_temp = concepts_nouns[i];
        if (concept_temp != null) {
            concept = concept + "-" + concept_temp;
        }
    }
    
    //add the verbs to create the concept
    console.log('This is the current concept:',concept);
    concepts_verbs = doco.sentences().verbs().out('array');
    for(var i=0; i<concepts_verbs.length; i++) {
        let concept_temp = concepts_verbs[i];
        if (concept_temp != null) {
            concept = concept + "-" + concept_temp;
        }
    }

    if (concept == ""){
        console.log(">>>>>>>>>Concept is still empty!!!", doco);
        
        //if we have not been able to find a concept we will use the whole sentence as the concept to ensure
        //that we get a unique entry for it and we don't overwrite all the time the "general" one
        concept = "-"+ question.replace("/ /g","-");
        console.log('This is the current concept:',concept);
        
        //message.text.replace("/ /g","-");
        /*
        let max = doco.sentences(0).terms().out('array').length;
        for(var i=0; i<max; i++){
            console.log('This is the current concept:',concept);
            //console.log(doco.terms(i).out('array'),'<>',doco.terms(i).out('tags')[0].tags);
            let tags = doco.terms(i).out('tags')[0].tags;
            let length = tags.length;
            for(var j=0; j<length; j++){
                if(tags[j]=='Verb'){
                    concept = doco.terms(i).out('array')[0];
                    console.log('This is the concept decided:',concept);
                }
            }
        } */
    }
    
    if (concept == "are"){
        let adjective = returnAdjective;
        concept = concept.concat(returnAdjective);
    }
    return concept;
}
/**
 * 
 * @param {*} doco 
 */

function returnAdjective (doco) {
    let concept = "general";
    console.log('This is the current concept A:',concept);
    let max = doco.sentences(0).terms().out('array').length;
    for(var i=0; i<max; i++){
        console.log('This is the current concept B:',concept);
        //console.log(doco.terms(i).out('array'),'<>',doco.terms(i).out('tags')[0].tags);
        let tags = doco.terms(i).out('tags')[0].tags;
        let length = tags.length;
        for(var j=0; j<length; j++){
            if(tags[j]=='Adjective'){
                concept = doco.terms(i).out('array')[0];
                console.log('This is the concept decided C:',concept);             
            }
        }
    }
    return concept;
}

/**
 * This is to show a summary of the information of the sentence.
 * 
 * @param {String} message 
 */
function summary(message){
    let doco = NLPLearn(message);
    console.log('=========>THIS IS THE SUMMARY OF ALL OF IT')
    //let terms = doco.sentences[0].terms;
    //let max = doco.terms.length;
    let max = doco.sentences(0).terms().out('array').length;
    console.log('=========>Array:', doco.out('array'));
    console.log('=========>Length of it:', max);
    
    for(var i=0; i<max; i++){
//            console.log(doco.terms(i).out('array').text,'||',doco.terms(i).tag);
        //console.log(doco.terms(i).list.Terms);            
        console.log(doco.terms(i).out('array'),'<>',doco.terms(i).out('tags')[0].tags);
        //console.log();
    }

    console.log('=====>THIS IS THE TOPIC: ', doco.topics().data());
    

}

// Load our training data
const trainingData = parseTrainingData("./trainingData.json");

// Keep the status if the next sentence is a learning one or one to answer normally
var isLearning = false;

// Keep the concept we will add the learning answer to
var concept = "world";
var question = "Whatever"

/**
 * This
 */
function train(){
    // For each of the labels in our training data,
    // train and generate the classifier
    var i = 0;
    Object.keys(trainingData).forEach((element, key) => {
        trainClassifier(classifier, element, trainingData[element].questions);
        i++;
        if (i === Object.keys(trainingData).length) {
            classifier.train();
            const filePath = './classifier.json';
            classifier.save(filePath, (err, classifier) => {
                if (err) {
                    console.error(err);
                }
                console.log('Created a Classifier file in ', filePath);
            });
        }
    });

}


//Train the bot
train();

// Configure the bot
// .* means match any message test
// The scopes we pass determine which kinds of messages we consider (in this case only direct message or mentions)
// handleMessage is the function that will run when the bot matches a message based on the text and scope criteria
Bot.hears('.*', scopes, handleMessage);

// Instantiate a chatbot using the previously defined template and API token
// Open a connection to Slack's real time API to start messaging
Bot.spawn({
    token: token
}).startRTM();


